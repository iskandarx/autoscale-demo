
var redisIo = require('socket.io-redis');

var redis = require('./redis');

function getIo(http) {
  var io = require('socket.io')(http);
  io.adapter(redisIo({
    pubClient: redis.clients.publish,
    subClient: redis.clients.subscribe,
  }));
  return io;
}

module.exports = getIo;