
var nconf = require('nconf');
var url = require('url');
var faker = require('faker');
var goby = require('goby').init();

var logger = require('./logger');

nconf.use('memory');

// Read argv and environment vars
nconf.argv().env();

// Set up defaults
nconf.defaults({
  "PORT":                      "3000",
  "CONSUL_PORT_8500_TCP_ADDR": false,
  "HOSTNAME":                  faker.helpers.slugify(goby.generate(['adj', 'suf'])).toLowerCase(),
  "CONSUL_AGENT":              "127.0.0.1",
  "CONSUL_SEED":               "192.168.99.100",
  "REDIS_URL":                 "tcp://192.168.99.100:6379",
  "REDIS_NAMESPACE":           "as-demo",
  "QUEUE_NAME":                'autoscale-demo',
  "SCALE_COOLDOWN_TIME":       120000, // Cooldown in ms
  "MINIMUM_SCALE_POLICY":      2,
});


// Let's parse env vars and get a CONSUL_SEED IP address
if (nconf.get('CONSUL_PORT_8500_TCP_ADDR')) {
  logger.debug('CONSUL_PORT_8500_TCP_ADDR', nconf.get('CONSUL_PORT_8500_TCP_ADDR'));
  nconf.set("CONSUL_SEED", nconf.get('CONSUL_PORT_8500_TCP_ADDR'));
}
logger.debug('CONSUL_SEED', nconf.get("CONSUL_SEED"));


// Let's parse env vars and get a REDIS_URL IP address
if (nconf.get('REDIS_PORT')) {
  nconf.set("REDIS_URL", nconf.get('REDIS_PORT'));
}

// Set up redis options
var redisOptions = url.parse(nconf.get("REDIS_URL"));
if (redisOptions.hasOwnProperty('auth') && redisOptions.auth) {
  redisOptions.password = redisOptions.auth.toString().replace('redistogo:', '');
}
nconf.set("REDIS_OPTIONS", redisOptions);
logger.debug('REDIS_OPTIONS', redisOptions);

var queueConnectionDetails = {
  package:   'ioredis',
  host:      redisOptions.hostname,
  password:  redisOptions.password,
  port:      redisOptions.port,
  database:  0,
  namespace: nconf.get("REDIS_NAMESPACE"),
  options:   { password: redisOptions.password }
};
nconf.set("REDIS_QUEUE_CONNECTION_DETAILS", queueConnectionDetails);
logger.debug('REDIS_QUEUE_CONNECTION_DETAILS', queueConnectionDetails);

logger.debug('REDIS_URL', nconf.get("REDIS_URL"));

module.exports = nconf;
