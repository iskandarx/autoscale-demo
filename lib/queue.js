
var nodeResque = require("node-resque");

var config = require('./config');
var logger = require('./logger');

var queueConnectionDetails = config.get('REDIS_QUEUE_CONNECTION_DETAILS');

function getQueue(jobs) {
  var queue = new nodeResque.queue({ connection: queueConnectionDetails }, jobs);

  queue.on('error', function (error) {
    logger.error(error);
  });

  queue.connect(function () {
    logger.debug('Connected to queue', queueConnectionDetails);
  });

  return queue;
}


module.exports = getQueue;