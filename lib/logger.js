var winston = require('winston');

// Set up logger
var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({ level: 'debug'})
  ]
});

module.exports = logger;