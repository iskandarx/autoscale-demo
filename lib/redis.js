
var redis = require('redis');

var config = require('./config');

var redisOptions = config.get('REDIS_OPTIONS');
var queueConnectionDetails = config.get('REDIS_QUEUE_CONNECTION_DETAILS');

var publish = redis.createClient(redisOptions.port, redisOptions.hostname,
  {auth_pass: redisOptions.password, return_buffers: true});
var subscribe = redis.createClient(redisOptions.port, redisOptions.hostname,
  {auth_pass: redisOptions.password, return_buffers: true});

module.exports = {
  redis: redis,
  clients: {
    publish: publish,
    subscribe: subscribe,
  },
  queueConnectionDetails: queueConnectionDetails,
};
