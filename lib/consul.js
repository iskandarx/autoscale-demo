var Promise = require("bluebird");

var config = require('./config');
var logger = require('./logger');

var consul = require('consul')({
  host: config.get("CONSUL_AGENT"),
});
Promise.promisifyAll(consul.agent.check);
Promise.promisifyAll(consul.health);
Promise.promisifyAll(consul.kv);

consul.agent.join(config.get("CONSUL_SEED"), function(err) {
  if (err) {
    logger.error('Failed to connect to CONSUL_SEED:', config.get("CONSUL_SEED"));
    throw err;
  }
  logger.info('Consul agent joined cluster', config.get("CONSUL_SEED"));
});

module.exports = consul;