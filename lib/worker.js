
var nodeResque = require("node-resque");

var config = require('./config');

var queueConnectionDetails = config.get('REDIS_QUEUE_CONNECTION_DETAILS');

function getWorker(workerName, jobs) {

  var workerDefinition = {
    name: workerName,
    connection: queueConnectionDetails,
    queues: [
      config.get('QUEUE_NAME'),
    ],
  };
  var worker = new nodeResque.worker(workerDefinition, jobs);

  worker.on('error', function (error) {
    logger.error(error);
  });

  return worker;
}

module.exports = getWorker;