
var _ = require('lodash');
var express = require('express');
var app = express();
var http = require('http').Server(app);

var logger = require('./lib/logger');
var config = require('./lib/config');
var io = require('./lib/socket-io')(http);


function getServerIp() {
    var os = require('os');
    var ifaces = os.networkInterfaces();
    var values = Object.keys(ifaces).map(function(name) {
        return ifaces[name];
    });
    values = [].concat.apply([], values).filter(function(val){
        return val.family == 'IPv4' && val.internal == false;
    });

    return values.length ? values[0].address : '0.0.0.0';
}

//var workerName = shortid.generate();
//var workerName = moniker.choose();
//var workerName = config.get("HOSTNAME") + ':' + process.pid;
var workerName = getServerIp() + ':' + config.get('PORT');

logger.debug('My name is ' + workerName);

app.get('/', function(req, res){
    res.send(
      '<h1>This is the page for a worker named ' +
      '<code>' + workerName+ '</code></h1>' +
      '<h2>Hostname: <em>' + config.get('HOSTNAME') + '</em></h2>');
});

http.listen(config.get("PORT"), function(){
    logger.debug('listening on *:' + config.get("PORT"));
});

var service = {
    name: "worker",
    id: workerName,
    tags: [
        "worker"
    ],
};

var consul = require('./lib/consul');

var checkTtl = '4s';
var checkInterval = 3000;

consul.agent.service.register(service, function(err) {
    if (err) {
        logger.error('Error registering consul agent', err);
        return;
    }
    logger.info('Consul service registered', service);

    var check = {
        name: workerName + '-check',
        service_id: workerName,
        ttl: checkTtl,
        status: 'passing',
    };
    consul.agent.check.registerAsync(check).
      then(function () {
          logger.info('Consul check registered', check);
          setInterval(function () {
              //logger.debug('Sending health check pass');
              consul.agent.check.pass(check.name, function (err) {
                  if (err) {
                      logger.error('Error sending check pass', err, check);
                  }
              });
          }, checkInterval);
      }).
      catch(function (err) {
          logger.error('Error registering consul check', err);
      });
});

function notify(message) {
    io.emit('chat message', {
        name: workerName,
        message: message,
    });
    logger.info(message);
}

var jobs = {
    "add": {
        perform: function(msg, workTime, callback){
            var startTime = new Date();
            var jobId = msg;
            setTimeout(function() {
                var endTime = new Date() - startTime;
                notify("'" + jobId + "' result after " + endTime + "ms");
                callback(null, 'ok');
            }, workTime);
        }
    }
};

var worker = require('./lib/worker')(workerName, jobs);

worker.connect(function() {
    worker.workerCleanup(); // optional: cleanup any previous improperly shutdown workers on this host
    worker.start();

    var joinMessages = [
        "Hello, I've joined",
        "Hello, is it me you're looking for?",
        'Never fear, a new node is here!',
        'Greetings and Salutations',
        "What's up, y'all?",
        "I have arrived to assist you",
        "Zug Zug!",
        "In the rear with the gear!",
        "I hunger for battle...",
        "How may I help?",
        "Your command?",
    ];
    notify(_.sample(joinMessages));
});

/**
 * Handle signals
 */
process.stdin.resume(); //so the program will not close instantly

function doCleanup(callback) {
    notify("Exiting...");
    consul.agent.service.deregister(workerName, function(err) {
        //if (err) throw err;
        worker.end(callback);
    });
}
function exitHandler(options, err) {
    if (err) {
        logger.error(err.stack);
    }
    if (options.exit) {
        process.exit();
    }
    if (options.signal) {
        process.kill(process.pid, options.signal);
    }
}

//do something when app is closing
//process.on('exit', function() { logger.debug('exit'); });
process.once('exit', function() {
    logger.debug('exit');
    //doCleanup(exitHandler.bind(null, { }));
});

//catches ctrl+c event
//process.on('SIGINT', function() { logger.debug('SIGINT'); });
//process.on('SIGINT', exitHandler.bind(null, { exit: true }));
process.once('SIGINT', function() {
    logger.debug('SIGINT');
    doCleanup(exitHandler.bind(null, {
        exit: false
    }));
});

//process.on('SIGUSR2', function() { logger.debug('SIGUSR2'); });
process.once('SIGUSR2', function() {
    logger.debug('SIGUSR2');
    doCleanup(exitHandler.bind(null, {
        exit: false,
        signal: 'SIGUSR2'
    }));
});