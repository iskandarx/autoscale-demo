#!/bin/bash

set -eu
set -o pipefail
set -x

RAX_USER=${RAX_USER:-}
RAX_API_KEY=${RAX_API_KEY:-}
DEFAULT_GROUP_KEY=$(env LC_CTYPE=C LC_ALL=C tr -dc "a-z0-9" < /dev/urandom | head -c 4 || true)
GROUP_KEY=${1:-$DEFAULT_GROUP_KEY}

cat > /tmp/rax-auth << EOF
{"auth":{"RAX-KSKEY:apiKeyCredentials":{"username":"${RAX_USER}", "apiKey":"${RAX_API_KEY}"}}}
EOF

curl -s https://identity.api.rackspacecloud.com/v2.0/tokens -X 'POST' \
       -d @/tmp/rax-auth \
       -H "Content-Type: application/json" | jq .

TOKEN_DATA_FILE=/tmp/token-data
curl -s https://identity.api.rackspacecloud.com/v2.0/tokens -X 'POST' \
       -d @/tmp/rax-auth \
       -H "Content-Type: application/json" > $TOKEN_DATA_FILE

RAX_AUTH_TOKEN=$(cat $TOKEN_DATA_FILE | jq --raw-output .access.token.id)
RAX_TENANT_ID=$(cat $TOKEN_DATA_FILE | jq --raw-output .access.token.tenant.id)
RAX_AS_API_URL=$(cat $TOKEN_DATA_FILE | jq --raw-output -c '.access.serviceCatalog[] | select(.name | contains("autoscale")) | .endpoints[0].publicURL')
RAX_LB_API_URL=$(cat $TOKEN_DATA_FILE | jq --raw-output -c '.access.serviceCatalog[] | select(.name | contains("cloudLoadBalancers")) | .endpoints[0].publicURL')

function raxcurl {
    curl --silent -H "X-Auth-Token: $RAX_AUTH_TOKEN" -H "Accept: application/json" -H "Content-Type: application/json" "$@"
}


LB_NAME=lb-${GROUP_KEY}
GROUP_NAME=asg-${GROUP_KEY}
SERVER_NAME=node-${GROUP_KEY}

# Create a CLB
cat > /tmp/lb.json <<EOF
{
    "loadBalancer": {
        "name": "${LB_NAME}",
        "port": 80,
        "protocol": "HTTP",
        "virtualIps": [
          { "type": "PUBLIC" }
        ]
    }
}
EOF

raxcurl -X POST ${RAX_LB_API_URL}/loadbalancers -d@/tmp/lb.json > /tmp/lb-data.json
LB_ID=$(cat /tmp/lb-data.json | jq --raw-output .loadBalancer.id)

# Create the ASG
#  "OS-DCF:diskConfig" must be "MANUAL" or servers won't be created!
cat > /tmp/asg.json <<EOF
{
    "groupConfiguration": {
        "name": "${GROUP_NAME}",
        "minEntities": 0,
        "maxEntities": 16,
        "cooldown": 5,
        "metadata": {

        }
    },
    "launchConfiguration": {
        "args": {
            "loadBalancers": [
                {
                    "port": 80,
                    "loadBalancerId": ${LB_ID}
                }
            ],
            "server": {
                "name": "${SERVER_NAME}",
                "OS-DCF:diskConfig" : "MANUAL",
                "config_drive" : true,
                "flavorRef": "3",
                "key_name": "MBP",
                "imageRef": "09de0a66-3156-48b4-90a5-1cf25a905207",
                "user_data" : "I2Nsb3VkLWNvbmZpZw0KDQpwYWNrYWdlczoNCg0KICAgIC0gY3VybA0KICAgIC0gZ2l0DQoNCm91dHB1dCA6IHsgYWxsIDogJ3wgdGVlIC1hIC92YXIvbG9nL2Nsb3VkLWluaXQtb3V0cHV0LmxvZycgfQ0KDQpydW5jbWQ6DQogICAgLSAnXGN1cmwgLXNMIGh0dHBzOi8vYml0YnVja2V0Lm9yZy9pc2thbmRhcngvYXV0b3NjYWxlLWRlbW8vcmF3L21hc3Rlci9jbG91ZC1pbml0LnNoIHwgc3VkbyBiYXNoIC0n",
                "networks": [
                    {
                         "uuid": "00000000-0000-0000-0000-000000000000"
                    },
                    {
                         "uuid": "11111111-1111-1111-1111-111111111111"
                    }
                ]
            }
        },
        "type": "launch_server"
    },
    "scalingPolicies": [
         {
            "name": "hook-scale-up-01",
            "change": 1,
            "cooldown": 5,
            "type":"webhook"
         },
         {
            "name": "hook-scale-up-50pc",
            "changePercent": 50,
            "cooldown": 5,
            "type":"webhook"
         },
         {
            "name": "hook-scale-down-01",
            "change": -1,
            "cooldown": 5,
            "type":"webhook"
         },
         {
            "name": "hook-scale-down-50pc",
            "changePercent": -50,
            "cooldown": 5,
            "type":"webhook"
         },
         {
            "name": "hook-scale-to-12",
            "desiredCapacity": 12,
            "cooldown": 5,
            "type":"webhook"
         },
         {
            "name": "hook-scale-to-08",
            "desiredCapacity": 8,
            "cooldown": 5,
            "type":"webhook"
         },
         {
            "name": "hook-scale-to-06",
            "desiredCapacity": 6,
            "cooldown": 5,
            "type":"webhook"
         },
         {
            "name": "hook-scale-to-04",
            "desiredCapacity": 4,
            "cooldown": 5,
            "type":"webhook"
         },
         {
            "name": "hook-scale-to-02",
            "desiredCapacity": 2,
            "cooldown": 5,
            "type":"webhook"
         },
         {
            "name": "hook-scale-to-01",
            "desiredCapacity": 1,
            "cooldown": 5,
            "type":"webhook"
         },
         {
            "name": "hook-scale-to-00",
            "desiredCapacity": 0,
            "cooldown": 5,
            "type":"webhook"
         }
    ]
}
EOF

# Create the autoscale group
raxcurl -X POST $RAX_AS_API_URL/groups -d @/tmp/asg.json > /tmp/asg-data.json

cat /tmp/asg-data.json | jq .group.id

# Add Webhooks to each policy
sleep 10

## This giant list is irritating
SCALE_TO_TWELVE=$(cat /tmp/asg-data.json | jq --raw-output -c '.group.scalingPolicies[] | select(.name | contains("scale-to-12")) | .links[0].href')
SCALE_TO_EIGHT=$(cat /tmp/asg-data.json | jq --raw-output -c '.group.scalingPolicies[] | select(.name | contains("scale-to-08")) | .links[0].href')
SCALE_TO_SIX=$(cat /tmp/asg-data.json | jq --raw-output -c '.group.scalingPolicies[] | select(.name | contains("scale-to-06")) | .links[0].href')
SCALE_TO_FOUR=$(cat /tmp/asg-data.json | jq --raw-output -c '.group.scalingPolicies[] | select(.name | contains("scale-to-04")) | .links[0].href')
SCALE_TO_TWO=$(cat /tmp/asg-data.json | jq --raw-output -c '.group.scalingPolicies[] | select(.name | contains("scale-to-02")) | .links[0].href')
SCALE_TO_ONE=$(cat /tmp/asg-data.json | jq --raw-output -c '.group.scalingPolicies[] | select(.name | contains("scale-to-01")) | .links[0].href')
SCALE_TO_ZERO=$(cat /tmp/asg-data.json | jq --raw-output -c '.group.scalingPolicies[] | select(.name | contains("scale-to-00")) | .links[0].href')
SCALE_UP_HREF=$(cat /tmp/asg-data.json | jq --raw-output -c '.group.scalingPolicies[] | select(.name | contains("scale-up-01")) | .links[0].href')
SCALE_UP_50PC_HREF=$(cat /tmp/asg-data.json | jq --raw-output -c '.group.scalingPolicies[] | select(.name | contains("scale-up-50pc")) | .links[0].href')
SCALE_DOWN_HREF=$(cat /tmp/asg-data.json | jq --raw-output -c '.group.scalingPolicies[] | select(.name | contains("scale-down-01")) | .links[0].href')
SCALE_DOWN_50PC_HREF=$(cat /tmp/asg-data.json | jq --raw-output -c '.group.scalingPolicies[] | select(.name | contains("scale-down-50pc")) | .links[0].href')

raxcurl -X POST ${SCALE_TO_TWELVE}webhooks -d '[{ "name": "hook-scale-to-12-01" }]' | tee /tmp/hook-scale-to-12-data.json
raxcurl -X POST ${SCALE_TO_EIGHT}webhooks -d '[{ "name": "hook-scale-to-08-01" }]' | tee /tmp/hook-scale-to-08-data.json
raxcurl -X POST ${SCALE_TO_SIX}webhooks -d '[{ "name": "hook-scale-to-06-01" }]' | tee /tmp/hook-scale-to-06-data.json
raxcurl -X POST ${SCALE_TO_FOUR}webhooks -d '[{ "name": "hook-scale-to-04-01" }]' | tee /tmp/hook-scale-to-04-data.json
raxcurl -X POST ${SCALE_TO_TWO}webhooks -d '[{ "name": "hook-scale-to-02-01" }]' | tee /tmp/hook-scale-to-02-data.json
raxcurl -X POST ${SCALE_TO_ONE}webhooks -d '[{ "name": "hook-scale-to-01-01" }]' | tee /tmp/hook-scale-to-01-data.json
raxcurl -X POST ${SCALE_TO_ZERO}webhooks -d '[{ "name": "hook-scale-to-00-01" }]' | tee /tmp/hook-scale-to-00-data.json
raxcurl -X POST ${SCALE_UP_HREF}webhooks -d '[{ "name": "hook-scale-up-01-01" }]' | tee /tmp/hook-scale-up-01-data.json
raxcurl -X POST ${SCALE_UP_50PC_HREF}webhooks -d '[{ "name": "hook-scale-up-50pc-01" }]' | tee /tmp/hook-scale-up-50pc-data.json
raxcurl -X POST ${SCALE_DOWN_HREF}webhooks -d '[{ "name": "hook-scale-down-01-01" }]' | tee /tmp/hook-scale-down-01-data.json
raxcurl -X POST ${SCALE_DOWN_50PC_HREF}webhooks -d '[{ "name": "hook-scale-down-50pc-01" }]' | tee /tmp/hook-scale-down-50pc-data.json

# Wait a bit
echo "Waiting for LB and ASG..."
sleep 40

# Let's scale up to two
SCALE_UP_HOOK_URL=$(cat /tmp/hook-scale-to-2-data.json | jq --raw-output -c '.webhooks[0].links[] | select (.rel | contains ("capability")) | .href')
curl -X POST $SCALE_UP_HOOK_URL -i

exit 0

#Controller App
#
#* Show queue status
#* Add items to queue
#* If queue length hits threshold, trigger scale up event
#* If queue length goes below threshold, trigger scale down event
