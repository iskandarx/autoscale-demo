#!/bin/bash

echo
echo
echo "Creating resources..."
heat stack-create --template-file ./asg.yaml ${ENVIRONMENT_NAME} \
    -P ssh_keypair_name=$KEY_PAIR 2>&1 | grep -v InsecurePlatformWarning | grep -v SubjectAltNameWarning
echo "Done"
echo

echo "Waiting for resources. This may take a few minutes..."
while true; do
    STATUS=$(heat stack-show ${ENVIRONMENT_NAME} 2>/dev/null | grep stack_status | grep -v reason | awk '{print $4}' 2>/dev/null)
    echo "Stack Status: $STATUS"
    if [ "$STATUS" = "CREATE_COMPLETE" ]; then
       echo "Done" && break;
    fi
    sleep 15
done
echo

#heat resource-list ${ENVIRONMENT_NAME} 2>/dev/null
#heat stack-show ${ENVIRONMENT_NAME} 2>/dev/null

# Store outputs as JSON
heat output-show --all ${ENVIRONMENT_NAME} 2>/dev/null > ${ENVIRONMENT_NAME}-output.json

# Extract the master server IP and LB IP
MASTER_IP=$(cat ${ENVIRONMENT_NAME}-output.json | jq --raw-output -c '.[] | select(.output_key | contains("master_public_ip")) | .output_value')
CLUSTER_IP=$(cat ${ENVIRONMENT_NAME}-output.json | jq --raw-output -c '.[] | select(.output_key | contains("cluster_public_ip")) | .output_value')

echo
echo "------------------------------------------------------"
echo "MASTER IP: ${MASTER_IP}"
echo "CLUSTER IP: ${CLUSTER_IP}"
echo "------------------------------------------------------"
echo

# Push Heat output data to Key-Value store
echo "Updating metadata..."
curl --silent -X PUT -d@${ENVIRONMENT_NAME}-output.json \
	http://${MASTER_IP}:8500/v1/kv/heat/outputs
echo

#MASTER_IP=8.8.8.8
#CLUSTER_IP=4.4.4.4

# Update DNS
export RAX_USERNAME
export RAX_API_KEY

echo "Updating DNS for ${MASTER_NAME}: ${MASTER_IP}..."
ansible-playbook -i /dev/null \
  -e base_name=${BASE_DOMAIN} \
  -e name=${MASTER_NAME} \
  -e ip=${MASTER_IP} \
  ansible/main.yaml
echo "Done"
echo

echo "Updating DNS for ${CLUSTER_NAME}: ${CLUSTER_IP}..."
ansible-playbook -i /dev/null \
  -e base_name=${BASE_DOMAIN} \
  -e name=${CLUSTER_NAME}\
  -e ip=${CLUSTER_IP} \
  ansible/main.yaml
echo "Done"
echo

echo "Tidying up..."
rm -f *.json
echo "Done"
echo

echo
echo
echo "------------------------------------------------------"
echo "Master: http://${MASTER_NAME}.${BASE_DOMAIN}/"
echo "  * http://${MASTER_IP}/"
echo "Consul UI: http://${MASTER_NAME}.${BASE_DOMAIN}:8500/ui"
echo " * http://${MASTER_IP}:8500/ui"
echo "Load-Balanced Nodes: http://${CLUSTER_NAME}.${BASE_DOMAIN}/"
echo " * http://${CLUSTER_IP}/"
echo "------------------------------------------------------"
echo
echo




