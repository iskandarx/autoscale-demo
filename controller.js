
var express = require('express');
var app = express();
var http = require('http').Server(app);
var _ = require('lodash');
var Promise = require("bluebird");
var request = Promise.promisifyAll(require('request'));

var logger = require('./lib/logger');
var config = require('./lib/config');
var io = require('./lib/socket-io')(http);

//logger.debug(process.env);

app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

app.get('/favicon.ico', function(req, res){
    res.sendFile(__dirname + '/favicon.ico');
});

io.on('connection', function(socket) {
    logger.log('a user connected');
    socket.on('disconnect', function(){
        logger.log('user disconnected');
    });
});

http.listen(config.get("PORT"), function(){
    logger.debug('listening on *:' + config.get("PORT"));
});


var workerName = 'controller';
var serviceDefinition = {
    name: workerName,
    id: config.get("HOSTNAME"),
    tags: [
        workerName
    ]
};

var consul = require('./lib/consul');

consul.agent.service.register(serviceDefinition, function(err) {
    if (err) {
        logger.error('Error registering consul agent', err);
        return;
    }
    logger.info('Consul service registered', serviceDefinition);
});

var queue = Promise.promisifyAll(require('./lib/queue')({ }));

var queueName = config.get('QUEUE_NAME');
var scaleCoolDownTime = config.get('SCALE_COOLDOWN_TIME'); //120000; // Cooldown in ms
var minimumScalePolicy = config.get('MINIMUM_SCALE_POLICY');

var queueLengthQuietThreshold = 10;
var queueLengthLoudThreshold = 100;
var lastScaleCommandTime = Number.MIN_VALUE;
var queueCheckInterval = 5000;

// How long to work in each job
var jobTtl = 6000;

// Store node IDs in this array
var nodeIds = [];

function notify(msg, name) {
    var message = msg;
    if ( ! _.isObject(message)) {
        message = {
            name: name || workerName,
            message: message,
        }
    }
    io.emit('chat message', message);
    logger.info(message);
}

function getActiveWorkers() {
    return consul.health.serviceAsync('worker').
      then(function (result) {
          //logger.debug('health.service result 2', JSON.stringify(result));
          var activeWorkers = [];
          var workerIds = [];
          _.forEach(result, function (worker) {
              if (worker.Service.Service !== 'worker') {
                  return;
              }
              // Look for failing 'serfHealth' checks
              var failingSerfChecks = _.where(worker.Checks, {
                  CheckID:     "serfHealth",
                  Status:      "critical",
              });
              //logger.debug('failingSerfChecks', JSON.stringify(failingSerfChecks));
              // Look for passing 'worker' check
              var checks = _.where(worker.Checks, {
                  ServiceName: "worker",
                  Status:      "passing",
              });
              //logger.debug('Checks', JSON.stringify(checks));
              if (failingSerfChecks.length === 0 && checks.length > 0) {
                  activeWorkers.push(worker);
                  workerIds.push(checks[0].ServiceID);
              }
          });
          return {
              workers: activeWorkers,
              ids:     workerIds,
          };
      }).
      catch(function (err) {
          logger.error(err);
      });
}

function getQueueLength() {
    return queue.lengthAsync(queueName);
}

setInterval(function() {
    getActiveWorkers().
      then(function (workers) {
          //logger.debug('getActiveWorkers', JSON.stringify(workers));
          nodeIds = workers.ids; // _.pluck(workers.list, "Node.Node");
          logger.debug('getActiveWorkers nodeIds', JSON.stringify(nodeIds));
          //logger.debug('getActiveWorkers IDs', JSON.stringify(workers.ids));
          io.emit('nodeIds', nodeIds);
          return workers;
      }).
      then(getQueueLength).
      then(function (len) {
          logger.debug("Queue Length: " + len);
          io.emit('queueLength', len);
          return len;
      }).
      then(makeScalingDecision).
      catch(function (err) {
          logger.error(err);
      });
}, queueCheckInterval);


function makeScalingDecision(queueLength) {
    var timeSinceLastScaleCommand = new Date() - lastScaleCommandTime;
    logger.debug('timeSinceLastScaleCommand', timeSinceLastScaleCommand, scaleCoolDownTime);
    if (timeSinceLastScaleCommand < scaleCoolDownTime) {
        return true;
    }
    if (queueLength < queueLengthQuietThreshold) {
        if (nodeIds.length == minimumScalePolicy) {
            return true;
        }
        notify("Queue length is below threshold " + queueLengthQuietThreshold + " (" + queueLength + ")" +
          "; time since last scale command: " + timeSinceLastScaleCommand + "ms");
        var minString = ("0" + minimumScalePolicy.toString()).slice(-2);
        return scale('to-' + minString);
    }
    if (queueLength > queueLengthLoudThreshold) {
        notify("Queue length is over threshold " + queueLengthLoudThreshold + " (" + queueLength + ")" +
          "; time since last scale command: " + timeSinceLastScaleCommand + "ms");
        return scale('up-100pc');
    }
}

function parseHeatOutputs(target) {
    return function(result) {
        if ( ! result) {
            notify("Unable to find webhook URLs in key-value store. Target: " + target);
            // Return a dummy response
            return 'http://localhost:3000';
            //throw new Error('No Webhook URLs');
        }

        logger.debug('kv result', result);
        var outputs = JSON.parse(result.Value);

        var hookName = 'scale-' + target + '-webhook';
        var validHooks = _.filter(outputs, function (output) {
            return output.hasOwnProperty('output_key') && output.output_key.match(/webhook$/);
        });
        logger.debug('validHooks', _.pluck(validHooks, 'output_key'));
        var matchingHook = _.find(validHooks, {output_key: hookName});
        logger.debug('matchingHook', matchingHook);
        if (!matchingHook) {
            notify("Unknown SCALE request: " + target + ". Valid scale types are: " +
              _.pluck(validHooks, 'output_key').join(', '));
            throw new Error('Unknown SCALE request');
        }
        notify("Sending SCALE request: " + target);

        logger.debug('posting to webhook', matchingHook.output_value);

        return matchingHook.output_value;
    }
}

function scale(target) {
    return consul.kv.getAsync('heat/outputs').
      then(parseHeatOutputs(target)).
      then(request.post).
      then(function(response, body) {
          logger.debug('response', response.statusCode, response.headers);
          lastScaleCommandTime = new Date();
          return response;
      }).
      catch(function(err) {
         logger.error(err);
      });
}

io.on('connection', function(socket) {
    socket.on('chat message', function(payload) {
        logger.debug("Message payload,", payload);
        var msg = payload.message;
        notify(payload);

        if (msg.match(/^\/ADD (\d+) JOB/i)) {
            var matches = msg.match(/^\/ADD (\d+)/i);
            var count = parseInt(matches[1], 10);
            _.times(count, function(n) {
                queue.enqueue(queueName, "add", ["JOB " + n, jobTtl]);
            });
            notify("QUEUED " + count + " JOBS");
            return;
        }
        if (msg.match(/^\/JOB/i)) {
            queue.enqueue(queueName, "add", [msg, jobTtl]);
            notify("QUEUED: " + msg);
            return;
        }
        if (msg.match(/^\/LEN/i)) {
            getQueueLength().then(function(result) {
                notify("LENGTH: " + result);
            });
            return;
        }
        if (msg.match(/^\/SET WORK TIME/i)) {
            var matches = msg.match(/^\/SET WORK TIME (\d+)/i);
            var num = parseInt(matches[1], 10);
            jobTtl = num;
            notify("WORK TIME SET TO: " + jobTtl + "ms (applies to new jobs)");
            return;
        }
        if (msg.match(/^\/SET COOLDOWN/i)) {
            var matches = msg.match(/^\/SET COOLDOWN (\d+)/i);
            var num = parseInt(matches[1], 10);
            scaleCoolDownTime = num;
            notify("SCALING COOLDOWN TIME SET TO: " + scaleCoolDownTime + "ms");
            return;
        }
        if (msg.match(/^\/SET MINIMUM/i)) {
            var matches = msg.match(/^\/SET MINIMUM (\d+)/i);
            minimumScalePolicy = parseInt(matches[1], 10);
            notify("MINIMUM SCALE POLICY SET TO: " + minimumScalePolicy + "");
            return;
        }
        if (msg.match(/^\/SCALE/i)) {
            var scaleTarget = false;
            if (msg.match(/^\/SCALE UP$/i)) {
                scaleTarget = "up-01";
            }
            if (msg.match(/^\/SCALE DOWN$/i)) {
                scaleTarget = "down-01";
            }
            var parts = msg.match(/^\/SCALE (.+)/i);
            //logger.debug(parts);
            if (parts) {
                scaleTarget = parts[1].toLowerCase();
            }
            if ( ! scaleTarget) {
                return;
            }
            scale(scaleTarget);
        }

        if (msg.match(/^\/NODE/i)) {
            notify("NODES (" + nodeIds.length + "): " + nodeIds.join(', ') + "");
            return;
        }
    });
});

/**
 * Handle signals
 */
process.stdin.resume(); //so the program will not close instantly

function doCleanup(callback) {
    notify("END OF LINE");
    queue.end(callback);
    consul.agent.service.deregister(workerName, function(err) {
        //if (err) throw err;
    });
}
function exitHandler(options, err) {
    if (err) {
        logger.error(err.stack);
    }
    if (options.exit) {
        process.exit();
    }
    if (options.signal) {
        process.kill(process.pid, options.signal);
    }
}

//do something when app is closing
//process.on('exit', function() { logger.debug('exit'); });
process.once('exit', function() {
    logger.debug('exit');
    //doCleanup(exitHandler.bind(null, { }));
});

//catches ctrl+c event
//process.on('SIGINT', function() { logger.debug('SIGINT'); });
//process.on('SIGINT', exitHandler.bind(null, { exit: true }));
process.once('SIGINT', function() {
    logger.debug('SIGINT');
    doCleanup(exitHandler.bind(null, {
        exit: false,
        signal: 'SIGINT'
    }));
});

//process.on('SIGUSR2', function() { logger.debug('SIGUSR2'); });
process.once('SIGUSR2', function() {
    logger.debug('SIGUSR2');
    doCleanup(exitHandler.bind(null, {
        exit: false,
        signal: 'SIGUSR2'
    }));
});